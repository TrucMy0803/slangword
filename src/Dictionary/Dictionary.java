package Dictionary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Dictionary {
	
	final static String filePathSlangWord = "D:\\Project\\DictionaryConsole\\Data\\slang.txt";
	final static String filePathOriginal = "D:\\Project\\DictionaryConsole\\Data\\original.txt";
	final static String filePathHistory = "D:\\Project\\DictionaryConsole\\Data\\slangHistory.txt";
	public static Scanner input= new Scanner(System.in);
	public static HashMap<String, List<String>> slangWord = new HashMap<String, List<String>>();
	public static List<String> historySlangWord = new ArrayList();
	
	//get slang word history from slangHistory.txt
	public static void GetHistorySlangWord() {
		BufferedReader br = null;
        
        try {
        	// create file object
	        File file = new File(filePathHistory);
	        // create BufferedReader object from the File
	        br = new BufferedReader(new FileReader(file));
	        
	        String line;
	        
	        // read file line by line
	        while((line = br.readLine()) != null)
	        {
	            historySlangWord.add(line);
	        }
	        
	        br.close();
        }
        catch (Exception ex) {
        	System.out.println("Lỗi: "+ex);
        }
    }
	
	//get slang word from slang.txt
	public static void GetSlangWord() {
		BufferedReader br = null;
		
		try {
			// create file object
	        File file = new File(filePathSlangWord);
	        // create BufferedReader object from the File
	        br = new BufferedReader(new FileReader(file));
	        
	        String line;
	        
	     // read file line by line
	        while((line = br.readLine()) != null)
	        {
	            // split the line by :
                String[] part1 = line.split("`");
                String[] part2 = line.split("\\|");
                List<String> parts = Arrays.asList(part2);
                slangWord.put(part1[0], parts);
	        }
	        
	        UpdateFile();
	        
	        br.close();
		}
		catch (Exception ex) {
			System.out.println("Lỗi: "+ex);
		}
	}
	
	public static void UpdateHistory(){
		BufferedWriter bw = null;
		
        try {
            File file = new File(filePathHistory);
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            
            for (String word : historySlangWord) {
                fw.write(word + "\n");
            }
            
            fw.close();
            bw.close();
        }
        catch (Exception ex) {
            System.out.println("Lỗi: "+ex);
        }
    }
	
	public static void UpdateFile() {
		BufferedWriter bw = null;
		
		try {
            File file = new File(filePathOriginal);
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            
            for (String i: slangWord.keySet())
            {
                List<String> value = slangWord.get(i);
                
                for (int j = 0; j < value.size(); j++)
                {
                	if (value.get(j).contains("`")) {
                		fw.write(value.get(j));
                	}
                	else {
                		if (j > 0) {
                			fw.write(value.get(j));
                		}
                		else {
                			fw.write(i +"`");
                    		fw.write(value.get(j));
                		}
                	}
                    if (j + 1 < value.size()) {
                    	fw.write("|");
                    }
                }
                
                fw.write("\n");
            }
            
            fw.close();
            bw.close();
        }
        catch (Exception ex) {
            System.out.println("Lỗi: "+ex);
        }
	}

	public static void FindSlangWord() {
        System.out.print("Nhập từ bạn muốn tìm kiếm: ");
        
        String word = input.next();
        word = word.toUpperCase();
        
        List<String> list = slangWord.get(word);
        historySlangWord.add(word);
        
        UpdateHistory();
        
        if (list == null) {
        	System.out.print("Không tìm thấy \n");
        }
        else {
        	System.out.println(list);
        }
        System.out.print("-------------------------------------\n");
    }
	
	public static void FindDefinition() {
        System.out.println("Nhập từ bạn cần tìm kiếm: ");
        
        String word = input.next();
        historySlangWord.add(word);
        
        UpdateHistory();
        
        List<String> list = new ArrayList();
        
        for (String i: slangWord.keySet())
        {
            if (slangWord.get(i).toString().contains(word))
            {
                list.add(i);
            }

        }
        
        if (list.isEmpty()) {
        	System.out.print("Không tìm thấy \n");
        }
        else {
        	System.out.println(list);
        }
        System.out.print("-------------------------------------\n");
    }
	
	public static void ShowListHistory(){
        System.out.println("Danh sách bạn đã tìm kiếm: ");
        
        for (String word : historySlangWord)
        {
            System.out.println(word);
        }
        System.out.print("-------------------------------------\n");
    }
	
	 public static void AddSlangWord() {
	        System.out.println("Nhập từ bạn cần thêm: ");
	        String key = input.next();
	        
	        System.out.println("Nhập definition: ");
	        String value = input.next();
	        
	        key = key.toUpperCase();
	        List<String> listValue=new ArrayList();
	        
	        
	        
	        if (slangWord.containsKey(key)) {
	            System.out.println("1. Bạn có muốn ovewrive");
	            System.out.println("2. Bạn có muốn duplicate ra 1 slang word mới");
	            int overwrite = input.nextInt();
	            
	            if (overwrite == 1) {
	            	listValue.add(value);
	            	slangWord.put(key, listValue);
	            }
	            else if (overwrite == 2)
	            {
	                List<String> i = slangWord.get(key);
	               
	                for (String j:i)
	                {
	                	if (j.contains("`")) {
	                		String[] j2 = j.toString().split("`");
	                		listValue.add(j2[1]);
	                	}
	                	else {
	                		listValue.add(j);
	                	} 
	                }
	                listValue.add(value);
	                slangWord.put(key, listValue);
	            }
	            UpdateFile();
	        }
	        else {
	        	listValue.add(value);
	            slangWord.put(key, listValue);
	            UpdateFile();
	        }
	        System.out.println("Bạn đã thêm từ mới vào từ điển");
	        System.out.print("-------------------------------------\n");
	    }
	 
	 public static void EditSlangWord() {
	    	System.out.print("-------------------------------------\n");
	        System.out.print("Nhập từ bạn muốn sửa: ");
	        
	        String word = input.next();
	        word = word.toUpperCase();
	        
	        if (slangWord.containsKey(word))
	        {
	            List<String> oldValue = slangWord.get(word);
	            List<String> newValue = new ArrayList();
	            
	            for (String i:oldValue)
	            {
	                newValue.add(i);
	            }
	            
	            int count = 1;
	            
	            if (oldValue.size() > 1) {
	            	for (String i: oldValue)
		            {
		                System.out.println(count+ "." + i);
		                count++;
		            }
		            
		            System.out.println("Bạn muốn thay đổi từ nào ở trên:  ");
		            int wordChange = input.nextInt();
		            newValue.remove(wordChange - 1);
	            }
	            else {
	            	 newValue.remove(0);
	            }
	            
	            System.out.print("Nhập defition bạn muốn thay đổi : ");
	            String value =input.next();
	            
	            newValue.add(value);
	            slangWord.put(word, newValue);
	            System.out.print("Bạn đã thay đổi thành công\n");
	            
	            UpdateFile();
	        }
	        else {
	        	 System.out.println("Từ này không tồn tại\n");
	        }
	        System.out.print("-------------------------------------\n");
	    }
	 
	 public static void DeleteSlangWord() {
		 System.out.println("Nhập từ bạn muốn xóa: ");
	     String word = input.next();
	     word = word.toUpperCase();
	     
	     if (slangWord.containsKey(word))
	     {
	         System.out.println("Bạn muốn xóa nó: (Y/N) ");
	         String cfm = input.next();
	         
	         if (cfm.equals("Y") || cfm.equals("y")) {
	        	 slangWord.remove(word);
	        	 UpdateFile();
	    	     System.out.print("Bạn đã xóa thành công\n");
	         }
	     }
	     
	     
	     System.out.print("-------------------------------------\n");
    }
	 
	 public static void ResetSlangWordOrinal() {
    	slangWord.clear();
    	UpdateFile();
    	GetSlangWord();
    	System.out.print("Đã reset slang words gốc\n");
    	System.out.print("-------------------------------------\n");
    }
	 
	 public static String RandomSlangString() {
        int count = 0;
        Random random = new Random();
        int countRandom = random.nextInt(slangWord.size());
        String value = "";
        
        for (String i: slangWord.keySet()) {
            if (count == countRandom) {
                value = i;
                break;
            }
            else {
            	count++;
            }
        }
        
        return value;
    }
	 
	 public static void GameA() {
        Random rand = new Random();
        List<String> container = new ArrayList<String>();

        String a = RandomSlangString();
        String wordQuestion = a;
        List<String> word1 = slangWord.get(a);
        a = word1.get(rand.nextInt(word1.size()));
        container.add(a);
        String WordRight = a;
        
        String b = RandomSlangString();
        List<String> word2 = slangWord.get(b);
        b = word2.get(rand.nextInt(word2.size()));
        container.add(b);
        
        String c = RandomSlangString();
        List<String> word3 = slangWord.get(c);
        c = word3.get(rand.nextInt(word3.size()));
        container.add(c);
        
        String d = RandomSlangString();
        List<String> word4 = slangWord.get(d);
        d = word4.get(rand.nextInt(word4.size()));
        container.add(d);
        
        System.out.println("Nghĩa của từ này là: " + wordQuestion);
        
        a = container.get(rand.nextInt(container.size()));
        container.remove(a);
        String[] a1 = a.toString().split("`");
        System.out.println("A.  " + a1[1]);
        
        b = container.get(rand.nextInt(container.size()));
        container.remove(b);
        String[] b1 = b.toString().split("`");
        System.out.println("B.  " + b1[1]);
        
        c = container.get(rand.nextInt(container.size()));
        container.remove(c);
        String[] c1 = c.toString().split("`");
        System.out.println("C.  " + c1[1]);
        
        d = container.get(rand.nextInt(container.size()));
        container.remove(d);
        String[] d1 = d.toString().split("`");
        System.out.println("D.  " + d1[1]);
        
        
        System.out.println("Đáp án của bạn là: ");
        String answer = input.next();
        answer = answer.toUpperCase();

        if ( (answer.equals("A")) && a == WordRight) {
        	System.out.println("Chúc mừng bạn đã chọn đúng");
        }
        else if ((answer.equals("B")) && b == WordRight) {
        	System.out.println("Chúc mừng bạn đã chọn đúng");
        }
        else if ((answer.equals("C")) && c == WordRight) {
        	System.out.println("Chúc mừng bạn đã chọn đúng");
        }
        else if ((answer.equals("D")) && d == WordRight) {
        	System.out.println("Chúc mừng bạn đã chọn đúng");
        }
        else System.out.println("Rất tiếc bạn đã trả lời sai rồi. Câu trả lời đúng là " + WordRight);
    }
	 
	 public static void GameB() {
    	Random rand = new Random();
        List<String> container = new ArrayList<String>();

        String a = RandomSlangString();
        container.add(a);
        String WordRight = a;
        List<String> word1 = slangWord.get(a);
        String[] a1 = word1.toString().split("`");
        String wordQuestion = a1[1].substring(0, a1[1].length() - 1);
        container.add(a);
        
        String b = RandomSlangString();
        container.add(b);
        
        String c = RandomSlangString();
        container.add(c);
        
        String d = RandomSlangString();
        container.add(d);
        
       System.out.println("Nghĩa của từ này là: " + wordQuestion);
       
       a = container.get(rand.nextInt(container.size()));
       container.remove(a);
       System.out.print("A. " + a + "\n");
       
       b = container.get(rand.nextInt(container.size()));
       container.remove(b);
       System.out.print("B. " + b + "\n");
       
       c = container.get(rand.nextInt(container.size()));
       container.remove(c);
       System.out.print("C. " + c + "\n");
       
       d = container.get(rand.nextInt(container.size()));
       container.remove(d);
       System.out.print("D. " + d + "\n");
        
        
       System.out.println("Đáp án của bạn là: ");
       String answer = input.next();
       answer = answer.toUpperCase();

       if ( (answer.equals("A")) && a == WordRight) {
       	System.out.println("Chúc mừng bạn đã chọn đúng");
       }
       else if ((answer.equals("B")) && b == WordRight) {
       	System.out.println("Chúc mừng bạn đã chọn đúng");
       }
       else if ((answer.equals("C")) && c == WordRight) {
       	System.out.println("Chúc mừng bạn đã chọn đúng");
       }
       else if ((answer.equals("D")) && d == WordRight) {
       	System.out.println("Chúc mừng bạn đã chọn đúng");
       }
       else System.out.println("Rất tiếc bạn đã trả lời sai rồi. Câu trả lời đúng là " + WordRight);
    }
	
	public static void main(String[] args) {
		int choiceNumber;
        
        GetSlangWord();
        GetHistorySlangWord();

        while (true) {
            System.out.println("Chào mừng bạn đến với slang word");
            System.out.println("Chúng tôi sẽ hỗ trợ cho bạn một số chức năng bên dưới: ");
            System.out.println("1. Tìm kiếm theo slang word.");
            System.out.println("2. Tìm kiếm theo definition.");
            System.out.println("3. Hiển thị danh sách tìm kiếm.");
            System.out.println("4. Thêm slang word mới.");
            System.out.println("5. Sửa slang word.");
            System.out.println("6. Xóa slang word.");
            System.out.println("7. Reset slang words gốc.");
            System.out.println("8. Random 1 slang word.");
            System.out.println("9. Trò chơi tìm slang word.");
            System.out.println("10. Trò chơi tìm definition.");
            System.out.println("Chọn 1 chức năng mà ban cần sử dụng");
            System.out.println("Chọn 0 để thoát");
            
            do {
            	System.out.print("-------------------------------------\n");
                System.out.println("Vui lòng chọn từ 1 đến 10: ");
                choiceNumber = input.nextInt();
            } while ((choiceNumber < 0) || (choiceNumber > 10));
             
            switch (choiceNumber) {
                case 1:
                	FindSlangWord();
                    break;
                case 2:
                	FindDefinition();
                    break;
                case 3:
                	ShowListHistory();
                	break;
                case 4:
                	AddSlangWord();
                	break;
                case 5: 
                	EditSlangWord();
                	break;
                case 6:
                	DeleteSlangWord();
                	break;
                case 7: 
                	ResetSlangWordOrinal();
                	break;
                case 8: 
                	System.out.println("Từ đã random là: ");
                	System.out.println(RandomSlangString());
                	System.out.print("-------------------------------------\n");
                	break;
                case 9:
                	GameA();
                	System.out.print("-------------------------------------\n");
                	break;
                case 10:
                	GameB();
                	System.out.print("-------------------------------------\n");
                	break;
                case 0:
                    System.out.println("Tạm biệt!");
                    System.exit(0); 
                    break;
            }
        }
	}
}
